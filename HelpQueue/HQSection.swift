//
//  HQSection.swift
//  HelpQueue
//
//  Created by Izaak Prats on 7/27/16.
//  Copyright © 2016 MakeSchool. All rights reserved.
//

import Foundation
import SwiftyJSON

class HQSection {
    let title: String
    let prettyTitle: String
    var requests = [HQRequest]()
    
    init(json: JSON) {
        self.title = json["title"].stringValue
        self.prettyTitle = json["pretty_title"].stringValue
        
        self.requests = json["_help_requestsK6kC7"]["edges"].arrayValue.map { return HQRequest(json: $0["node"]) }
    }
}