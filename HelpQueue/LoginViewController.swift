//
//  LoginViewController.swift
//  HelpQueue
//
//  Created by Izaak Prats on 7/27/16.
//  Copyright © 2016 MakeSchool. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON

class LoginViewController: UIViewController {
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBAction func loginTapped(sender: AnyObject) {
        loginUser { error in

            if error != nil {
                self.performSegueWithIdentifier("didLogin", sender: self)
            } else {
                print(error)
            }
            
        }
    }
    
    func loginUser(onComplete: NSError? -> Void) {
        let loginURL = NSURL(string: "https://www.makeschool.com/login")!
        let headers = [
            "__cfduid" : "dc511cd9f5fc6d71369cd55a6f0d834911469654740"
        ]
        let parameters = [
            "utf8" : "✓",
            "authenticity_token" : "MzyVrDdKO8p9XMsXJ6igwvVd27kTfKEazMUFtnQmzbLGNOMGroVgH5IdOP4lYaNu3zXGOx6eQJ35K1MAYffiNQ==",
            "user[email]" : "izaak@staff.makeschool.com",
            "user[password]" : "iaminsecure"
        ]
        
        Alamofire.request(.POST, loginURL, parameters: parameters, encoding: .URLEncodedInURL, headers: headers)
            .responseJSON { response in
                
                switch response.result {
                case .Success :
                    currentMentor.sessionId = response.response?.allHeaderFields["_makeschool_session"] as! String
                    break
                case .Failure(let error) :
                    onComplete(error)
                    break
                }
        }
    }
}