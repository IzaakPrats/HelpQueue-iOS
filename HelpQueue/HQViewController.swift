//
//  ViewController.swift
//  HelpQueue
//
//  Created by Izaak Prats on 7/27/16.
//  Copyright © 2016 MakeSchool. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class HQViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var selectedIndexPath: NSIndexPath!
    var sections = [HQSection]()
    var timer: NSTimer!
    
    @IBAction func refreshTapped(sender: AnyObject) {
        getHelpQueue()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let cookie = NSHTTPCookie(properties: [
            NSHTTPCookieDomain : "www.makeschool.com",
            NSHTTPCookiePath : "/",
            NSHTTPCookieName : "_makeschool_session",
            NSHTTPCookieValue : "SXE3cjFqaENxUVBSZlpMZkluYytnc0dGaWFTMk1yaUNQU1orR0hXNlk1b2NIVDdvZjZtZ3JFcWZNM25LWVRYcWVhTjlWbnV4OE1uS2hSdDdMMmRXRi9VeEFWeit4V2JFSXhQbVltYTNUSHVubW5VT2dMTGlpcWhhaFRSWStkQlFQaUp3eDcwUHlqbWVEUHV6TytJSCtHdnlSSjdUMlRidi9PU0NEbCtVRC8rQ2xzazkwenBMZjlZeGcyb1FzeURaMi94Y1IxL1lBZVU4VVBVc2d0elFaMlJhQzZRa1dRUUo4bnhYMGVsMEJkTkFTVTRBMkFHZXY1WmYzMndJTnlXclA5K2RKK0kzU1B1dXBUTHRGWDdGTHNTalUwWjV6b2lzejRCcXNmVmV3RXJNUktIWVBGdno1S1ZVSlJUSVZnUUo3YVNnL05HMkwvbng4aWUrY2RoOU1nWEl2bUptMFVUK2ZYQVNXVW5HODloa2xlaEJiWnNuYW4vKytIN2pCSEd1MkJwZGxwOEFCUjRUazI1MWg1YlE5ZUJDWFFmcTZ0eTNmZ012MTZSanJMbz0tLVhzTWdxZmtOWHNaWHYrUHMxbWFHdnc9PQ%3D%3D--656620edcd8c94049db44209396f09576dbe3dad"
        ])!
        
        Alamofire.Manager.sharedInstance.session.configuration.HTTPCookieStorage?.setCookies([cookie], forURL: HQBaseURL, mainDocumentURL: nil)
        
        getHelpQueue()
    }

    func getHelpQueue() {
        let request = Alamofire.request(.POST, HQURLString, parameters: HQParameters, encoding: .JSON, headers: HQHeaders)
        request.responseJSON { response in
                switch response.result {
                    
                case .Success :
                    if let json = response.result.value {
                        let value = JSON(json)
                        
                        let sections = value["data"]["viewer"]["user"]["_admin_queuesKejfP"]["edges"].arrayValue.map { return HQSection(json: $0["node"]) }
                        
                        self.sections = sections
                        self.tableView.reloadData()
                    }
                    
                    break
                    
                case .Failure(let error) :
                    print("DATA : \(JSON(response.data!)) ERROR: \(error.localizedDescription)")
                    
                    break
                }
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let hqRequestViewController = segue.destinationViewController as! HQRequestViewController
        
        hqRequestViewController.request = sections[tableView.indexPathForSelectedRow!.section].requests[tableView.indexPathForSelectedRow!.row]
        
    }
}

extension HQViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sections.isEmpty ? "" : sections[section].title
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections.isEmpty ? 0 : sections[section].requests.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("HQCell", forIndexPath: indexPath)
        let request = sections[indexPath.section].requests[indexPath.row]
        
        cell.textLabel!.text = request.name
        cell.detailTextLabel!.text = request.location
        
        if let mentor = request.helped_by {
            cell.textLabel!.text = "\(request.name) | \(mentor)"
        }
        
        return cell
    }
}

