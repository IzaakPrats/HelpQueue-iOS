//
//  HQRequest.swift
//  HelpQueue
//
//  Created by Izaak Prats on 7/27/16.
//  Copyright © 2016 MakeSchool. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire

class HQRequest {
    let id: String!
    var isComplete: Bool
    var position: Int
    var helped_by: String!
    let descriptionText: String
    let name: String
    let location: String
    let createdAt: NSDate = NSDate()
    
    init(json: JSON) {
        print(json)
        
        self.id = json["id"].stringValue
        self.isComplete = json["is_complete"].boolValue
        self.position = json["position"].intValue
        self.descriptionText = json["description"].stringValue
        self.name = json["user"]["name"].stringValue
        self.location = json["user"]["seating_location"].stringValue
        
        let helpedByJSON = json["helped_by"]
        if helpedByJSON.error == nil {
            self.helped_by = helpedByJSON["name"].stringValue
        }
    }
    
    func claimRequest(onComplete: String? -> Void) {
        let parameters = [
            "query" : "mutation SetClaimedBy($input_0:SetClaimedByInput!) {\n  set_claimed_by(input:$input_0) {\n    clientMutationId,\n    ...F0\n  }\n}\nfragment F0 on SetClaimedByPayload {\n  request {\n    id,\n    queue {\n      id\n    },\n    helped_by {\n      id,\n      name\n    }\n  }\n}",
            "variables" : [
                "input_0" : [
                    "request" : self.id,
                    "state" : true,
                    "clientMutationId" : 4
                ]
            ]
        ]
        
        Alamofire.request(.POST, HQURL, parameters: parameters, encoding: .JSON, headers: HQHeaders)
            .responseJSON { response in
                // IDK LOL MY BFF JILL?

                if let value = response.result.value {
                    let json = JSON(value)
                    
                    self.helped_by = json["data"]["set_claimed_by"]["request"]["helped_by"]["name"].stringValue
                    onComplete(self.helped_by)
                }
                
                onComplete(nil)
        }
    }
    
    func completeRequest(onComplete: NSError? -> Void) {
        // REQUEST
//        {"query":"mutation CompleteHelpRequest($input_0:CompleteHelpRequestInput!,$scope_1:HelpRequestScope!) {\n  complete_help_request(input:$input_0) {\n    clientMutationId,\n    ...F0\n  }\n}\nfragment F0 on CompleteHelpRequestPayload {\n  user {\n    id,\n    _admin_queuesKejfP:admin_queues(first:999) {\n      edges {\n        cursor,\n        node {\n          id,\n          title,\n          pretty_title,\n          _help_requestsK6kC7:help_requests(first:99,scope:$scope_1) {\n            edges {\n              node {\n                id,\n                user {\n                  name,\n                  profile_image_url,\n                  seating_location,\n                  id\n                },\n                queue {\n                  id\n                },\n                description,\n                helped_by {\n                  id,\n                  name\n                },\n                is_complete,\n                created_at,\n                completed_at,\n                position\n              },\n              cursor\n            },\n            pageInfo {\n              hasNextPage,\n              hasPreviousPage\n            }\n          }\n        }\n      },\n      pageInfo {\n        hasNextPage,\n        hasPreviousPage\n      }\n    }\n  }\n}","variables":{"input_0":{"request":"QWNhZGVteTo6SGVscFJlcXVlc3QtODg1","clientMutationId":"5"},"scope_1":"OPEN"}}
        
        let parameters = [
            "query" : "mutation CompleteHelpRequest($input_0:CompleteHelpRequestInput!,$scope_1:HelpRequestScope!) {\n  complete_help_request(input:$input_0) {\n    clientMutationId,\n    ...F0\n  }\n}\nfragment F0 on CompleteHelpRequestPayload {\n  user {\n    id,\n    _admin_queuesKejfP:admin_queues(first:999) {\n      edges {\n        cursor,\n        node {\n          id,\n          title,\n          pretty_title,\n          _help_requestsK6kC7:help_requests(first:99,scope:$scope_1) {\n            edges {\n              node {\n                id,\n                user {\n                  name,\n                  profile_image_url,\n                  seating_location,\n                  id\n                },\n                queue {\n                  id\n                },\n                description,\n                helped_by {\n                  id,\n                  name\n                },\n                is_complete,\n                created_at,\n                completed_at,\n                position\n              },\n              cursor\n            },\n            pageInfo {\n              hasNextPage,\n              hasPreviousPage\n            }\n          }\n        }\n      },\n      pageInfo {\n        hasNextPage,\n        hasPreviousPage\n      }\n    }\n  }\n}",
            "variables" : [
                "input_0" : [
                    "request" : self.id,
                    "clientMutationId" : 5
                ],
                "scope_1" : "OPEN"
            ]
        ]
        
        Alamofire.request(.POST, HQURL, parameters: parameters, encoding: .JSON, headers: HQHeaders)
            .responseJSON { response in
                // IDK LOL MY BFF JILL?
                
                if let value = response.result.value {
                    let json = JSON(value)
                    
                    if json.error == nil {
                        self.isComplete = true
                        onComplete(nil)
                    } else {
                        onComplete(json.error)
                    }
                }
                
                onComplete(NSError(domain: "DIDN'T WORK", code: 0, userInfo: nil))
        }

    }
}