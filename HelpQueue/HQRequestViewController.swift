
//
//  HQRequestViewController.swift
//  HelpQueue
//
//  Created by Izaak Prats on 7/27/16.
//  Copyright © 2016 MakeSchool. All rights reserved.
//

import Foundation
import UIKit

class HQRequestViewController: UIViewController {
    @IBOutlet weak var nameLocationLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var positionLabel: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var helpedByLabel: UILabel!
    
    @IBOutlet weak var claimRequest: UIBarButtonItem!
    @IBAction func claimRequestTapped(sender: AnyObject) {
        
        if claimRequest.title == "Complete" {
            self.request.completeRequest { error in
                if error == nil {
                    self.claimRequest.enabled = false
                    self.claimRequest.title = "Completed"
                } else {
                    // do the thing
                    print(error)
                }
            }
        } else {
            self.request.claimRequest { mentor in
                if let mentor = mentor {
                    self.helpedByLabel.text = mentor
                    self.claimRequest.title = "Complete"
                }
            }
        }
    }
    
    var request: HQRequest!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nameLocationLabel.text = "\(request.name) | \(request.location)"
        descriptionTextView.text = request.descriptionText
        timeLabel.text = "TIME TO BE IMPLEMENTED"
        positionLabel.text = "Position \(request.position)"
        
        if let mentor = request.helped_by {
            if mentor != "" {
                helpedByLabel.text = mentor
                helpedByLabel.hidden = false
                
                claimRequest.title = "Complete"
            }
        }
    }
}