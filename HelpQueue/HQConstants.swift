//
//  HQConstants.swift
//  HelpQueue
//
//  Created by Izaak Prats on 7/27/16.
//  Copyright © 2016 MakeSchool. All rights reserved.
//

import Foundation

let HQBaseURLString = "https://www.makeschool.com/"
let HQBaseURL = NSURL(string: HQBaseURLString)!

let HQURLString = "https://www.makeschool.com/graphql"
let HQURL = NSURL(string: HQURLString)!

let HQCookies = [
    "_makeschool_session" : currentMentor.sessionId
//    "_makeschool_session" : "SXE3cjFqaENxUVBSZlpMZkluYytnc0dGaWFTMk1yaUNQU1orR0hXNlk1b2NIVDdvZjZtZ3JFcWZNM25LWVRYcWVhTjlWbnV4OE1uS2hSdDdMMmRXRi9VeEFWeit4V2JFSXhQbVltYTNUSHVubW5VT2dMTGlpcWhhaFRSWStkQlFQaUp3eDcwUHlqbWVEUHV6TytJSCtHdnlSSjdUMlRidi9PU0NEbCtVRC8rQ2xzazkwenBMZjlZeGcyb1FzeURaMi94Y1IxL1lBZVU4VVBVc2d0elFaMlJhQzZRa1dRUUo4bnhYMGVsMEJkTkFTVTRBMkFHZXY1WmYzMndJTnlXclA5K2RKK0kzU1B1dXBUTHRGWDdGTHNTalUwWjV6b2lzejRCcXNmVmV3RXJNUktIWVBGdno1S1ZVSlJUSVZnUUo3YVNnL05HMkwvbng4aWUrY2RoOU1nWEl2bUptMFVUK2ZYQVNXVW5HODloa2xlaEJiWnNuYW4vKytIN2pCSEd1MkJwZGxwOEFCUjRUazI1MWg1YlE5ZUJDWFFmcTZ0eTNmZ012MTZSanJMbz0tLVhzTWdxZmtOWHNaWHYrUHMxbWFHdnc9PQ%3D%3D--656620edcd8c94049db44209396f09576dbe3dad"
]

let HQHeaders = [
    "x-csrf-token" : "DxfTEtmXPfDurJUQnvHsI1OtNuc/807s044mT4/xO6/oDiwRYOOxS1/5HIraF2bfKq4vtWF5bk4ouq0cxYWP0w=="
]

let HQParameters = [
    "query" : "query AppQueries($scope_0:HelpRequestScope!) {\n  viewer {\n    ...F0\n  }\n}\nfragment F0 on Viewer {\n  user {\n    id,\n    name,\n    cohort {\n      product,\n      id\n    },\n    _admin_queuesKejfP:admin_queues(first:999) {\n      edges {\n        cursor,\n        node {\n          id,\n          title,\n          pretty_title,\n          _help_requestsK6kC7:help_requests(first:99,scope:$scope_0) {\n            edges {\n              node {\n                id,\n                user {\n                  name,\n                  profile_image_url,\n                  seating_location,\n                  id\n                },\n                queue {\n                  id\n                },\n                description,\n                helped_by {\n                  id,\n                  name\n                },\n                is_complete,\n                created_at,\n                completed_at,\n                position\n              },\n              cursor\n            },\n            pageInfo {\n              hasNextPage,\n              hasPreviousPage\n            }\n          }\n        }\n      },\n      pageInfo {\n        hasNextPage,\n        hasPreviousPage\n      }\n    }\n  }\n}",
    "variables" : [
        "scope_0" : "OPEN"
    ]
]
